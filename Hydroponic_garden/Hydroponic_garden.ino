#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2561_U.h>
#include <DHT.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "SSD1306.h"
#include <ArduinoOTA.h>

#define mqtt_server "192.168.0.58"
#define mqtt_user "homeassistant"
#define mqtt_password "2032"
#define mqtt_client "hydroponic_garden"

#define external_luminosity_topic "sensor/hydroponic_garden/external_luminosity"
#define substrate_moisture_topic "sensor/hydroponic_garden/substrate_moisture"
#define humidity_topic "sensor/hydroponic_garden/humidity"
#define air_temperature_topic "sensor/hydroponic_garden/air_temperature"
#define water_temperature_topic "sensor/hydroponic_garden/water_temperature"

#define DHTTYPE DHT11
#define DHTPIN D3
#define WATER_TEMP_PIN D7

WiFiClient espClient;
PubSubClient client(espClient);
Adafruit_TSL2561_Unified tsl = Adafruit_TSL2561_Unified(TSL2561_ADDR_LOW, 12345);
DHT dht(DHTPIN, DHTTYPE, 11);
OneWire oneWire(WATER_TEMP_PIN);
DallasTemperature DS18B20(&oneWire);
SSD1306 display(0x3c, D2, D1);
char temperatureString[6];

void setup(void) {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  DS18B20.begin();
  configureLuxSensor();
  displayLuxSensorDetails();
  setup_screen();
  setup_OTA();
  drawScreen();
}

void setup_screen() {
  display.init();
  display.flipScreenVertically();
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(mqtt_client);
}

bool connected = false;
void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(mqtt_client, mqtt_user, mqtt_password)) {
      Serial.println("connected");
      connected = true;
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void displayLuxSensorDetails(void) {
  sensor_t sensor;
  tsl.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" lux");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" lux");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" lux");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

void configureLuxSensor(void) {
  // tsl.setGain(TSL2561_GAIN_1X);      /* No gain ... use in bright light to avoid sensor saturation */
  // tsl.setGain(TSL2561_GAIN_16X);     /* 16x gain ... use in low light to boost sensitivity */
  tsl.enableAutoRange(true);            /* Auto-gain ... switches automatically between 1x and 16x */

  /* Changing the integration time gives you better sensor resolution (402ms = 16-bit data) */
  tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_13MS);      /* fast but low resolution */
  // tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_101MS);  /* medium resolution and speed   */
  // tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_402MS);  /* 16-bit data but slowest conversions */
}

void setup_OTA() {
  ArduinoOTA.setHostname(mqtt_client);
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

bool checkBound(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

long lastLuxMsg = 0;
float extLux = 0.0;
float luxDiff = 1.0;
void readLuminosity() {
  long now = millis();
  if (now - lastLuxMsg > 2000) {
    lastLuxMsg = now;

    sensors_event_t event;
    tsl.getEvent(&event);
    float newExtLux = event.light;
    if (checkBound(newExtLux, extLux, luxDiff)) {
      extLux = newExtLux;
      Serial.print("New luminosity: ");
      Serial.print(String(extLux).c_str());
      Serial.println(" lux");
      client.publish(external_luminosity_topic, String(extLux).c_str(), true);
    }
  }
}

long lastTempMsg = 0;
float airTemp = 0.0;
float diff = 1.0;
void readAirTemp() {
  long now = millis();
  if (now - lastTempMsg > 2000) {
    lastTempMsg = now;

    float newTemp = dht.readTemperature();
    if (checkBound(newTemp, airTemp, diff)) {
      airTemp = newTemp;
      Serial.print("New temperature:");
      Serial.println(String(airTemp).c_str());
      client.publish(air_temperature_topic, String(airTemp).c_str(), true);
    }
  }
}

long lastHumMsg = 0;
float hum = 0.0;
void readHumidity() {
  long now = millis();
  if (now - lastHumMsg > 2000) {
    lastHumMsg = now;

    float newHum = dht.readHumidity();

    if (checkBound(newHum, hum, diff)) {
      hum = newHum;
      Serial.print("New humidity:");
      Serial.println(String(hum).c_str());
      client.publish(humidity_topic, String(hum).c_str(), true);
    }
  }
}

float waterTemp = 0.0;
long waterLastMsg = 0;
float waterDiff = 0.25;
void readWaterTemp() {
  long now = millis();
  if (now - waterLastMsg > 2000) {
    waterLastMsg = now;

    DS18B20.requestTemperatures();
    float newTemp = DS18B20.getTempCByIndex(0);

    if (checkBound(newTemp, waterTemp, waterDiff)) {
      waterTemp = newTemp;
      Serial.print("New water temperature: ");
      Serial.println(String(waterTemp).c_str());
      client.publish(water_temperature_topic, String(waterTemp).c_str(), true);
    }
    delay(5000);
  }
}

String getConnectStatus() {
  if (connected) {
    return "Connected";
  } else {
    return "Disconnected";
  }
}

void display_message(String message) {
  display.clear();
  display.drawRect(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);

  display.setFont(ArialMT_Plain_10);
  display.drawString(10, 30, message);
}

void drawScreen() {
  Serial.println("Redrawing screen");
  display.clear();
  display.drawRect(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);

  display.setFont(ArialMT_Plain_10);
  display.drawString(4, 0, "IP: " + String(WiFi.localIP().toString()));
  display.drawString(4, 15, "MQTT: " + getConnectStatus());
  display.drawString(4, 30, "Climate: " + String((int) airTemp) + "C   " + String((int) hum) + "%");
  display.drawString(4, 45, "Lux: " + String((int) extLux) + " lx  H20: " + String((int) waterTemp) + "C");

  display.display();
  yield();
}

void loop() {
  ArduinoOTA.handle();
  if (!client.connected()) {
    connected = false;
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();

  readAirTemp();
  readHumidity();
  readLuminosity();
  readWaterTemp();
  drawScreen();

  delay(2000);
}
